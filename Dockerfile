FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive

RUN DEBIAN_FRONTEND=noninteractive apt-get -qy update && apt-get -y install \
	bc \
	build-essential \
	cpio \
	curl \
	git \
	libncurses5-dev \
	openssl \
	python \
	sudo \
	unzip \
	wget \
    rsync \
    automake \
    autoconf \
	zlib1g-dev \
	libbz2-dev \
	libreadline-dev \
	libffi-dev \
	liblzma-dev \
	nodejs \
	node-gyp \
	nodejs-dev \
	libssl1.0-dev \
    pv \
	npm \
	jq && rm -rf /var/lib/apt/lists/*

RUN npm install -g bower
